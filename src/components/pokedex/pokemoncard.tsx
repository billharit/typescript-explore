import Image from "next/image";
import React from "react";

export default function PokemonCard({
  name,
  index,
}: {
  name: string;
  index: number;
}) {
  const pokeIndex = ("000" + (index + 1)).slice(-3);
  return (
    <li className="flex flex-col bg-white shadow-md rounded-md text-center hover:scale-105 duration-100">
      <div className="relative aspect-square">
        <span className="absolute top-0 right-3 z-50 font-bold text-gray-600 text-5xl">
          #{pokeIndex}
        </span>
        <Image
          src={`https://assets.pokemon.com/assets/cms2/img/pokedex/full/${pokeIndex}.png`}
          layout="fill"
          alt={name}
        />
      </div>
      <div className="text-black py-6 capitalize font-semibold">{name}</div>
    </li>
  );
}
