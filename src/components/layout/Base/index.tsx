import * as React from "react";

import Footer from "@/components/layout/Footer";
import Header from "@/components/layout/Header";

export default function BaseLayout({
  children,
}: {
  children: React.ReactNode;
}) {
  // Put Header or Footer Here
  return (
    <>
      <Header />
      <div className="min-h-screen">{children}</div>
      <Footer />
    </>
  );
}
