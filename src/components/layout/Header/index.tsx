import Link from "next/link";
import * as React from "react";

const links = [
  { href: "/", label: "Home" },
  { href: "/achievement", label: "Prestasi" },
  { href: "/facilities", label: "Fasilitas" },
  { href: "/segores-pena", label: "Segores Pena" },
];

export default function Header() {
  return (
    <header className="sticky  top-0 z-50 shadow-md md:relative">
      <div className="flex layout justify-between py-6">
        <Link href="/">Home</Link>
        <Link href="/pokedex">Pokedex</Link>
      </div>
    </header>
  );
}
