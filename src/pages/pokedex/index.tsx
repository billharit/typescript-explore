import BaseLayout from "@/components/layout/Base";
import PokemonCard from "@/components/pokedex/pokemoncard";
import React, { useEffect, useState, useMemo } from "react";
import axios from "axios";
import { GetStaticProps } from "next";

const per_page = 9;

export default function PokedexPage({ PokemonList }: any) {
  const [pokemon, setPokemon] = useState(PokemonList);
  const [offset, setOffset] = useState(0);
  const [currentPage, setCurrentPage] = useState(1);
  // const totalCount = PokemonList.count;
  // const pageRange = [];

  // console.log("tes");

  // const paginationRange = () => {
  //   const totalPageCount = Math.ceil(totalCount / per_page);
  //   const wingSiblings = 2;
  //   let pageRange: number[] = [];

  //   if (currentPage - wingSiblings < 0) {
  //     for (let i = 1; i < 5; i + 1) {
  //       pageRange = [1, 2, 3];
  //     }
  //   }

  //   if (currentPage - wingSiblings < 0) {
  //     for (
  //       let i = currentPage - wingSiblings;
  //       i < currentPage + wingSiblings;
  //       i + 1
  //     ) {
  //       pageRange.push(i);
  //     }
  //   } else if (currentPage + wingSiblings > totalPageCount) {
  //     for (let i = currentPage - wingSiblings; i < totalPageCount; i + 1) {
  //       pageRange.push(i);
  //     }
  //   } else {
  //     for (
  //       let i = currentPage - wingSiblings;
  //       i < currentPage + wingSiblings;
  //       i + 1
  //     ) {
  //       pageRange.push(i);
  //     }
  //   }

  //   console.log(pageRange);
  // };

  useEffect(() => {
    setOffset(per_page * currentPage);
    fetchPokemon(
      `https://pokeapi.co/api/v2/pokemon?limit=${per_page}&offset=${offset}`
    );
    console.log("tes");
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [currentPage]);

  const fetchPokemon = async (url: string) => {
    const response = await axios(url);
    const nextPokemonList = response.data;
    setPokemon(nextPokemonList);
  };

  return (
    <BaseLayout>
      <main className="py-8">
        <section className="layout">
          <h2 className="py-8 text-6xl font-bold text-center w-full">
            Pokedex
          </h2>
          <ul className="grid grid-cols-1 gap-6 sm:grid-cols-2 lg:grid-cols-3">
            {pokemon.results.map(
              ({ name }: { name: string }, index: number) => (
                <PokemonCard key={index} index={index + offset} name={name} />
              )
            )}
          </ul>
          <div className="flex flex-row justify-between mt-8 border-t border-gray-600">
            <button
              disabled={!pokemon.previous}
              className="cursor-pointer border-t-2 border-transparent pt-4 pr-1 items-center text-sm font-medium text-gray-500 hover:text-gray-200 hover:border-gray-300"
              onClick={() => setCurrentPage(currentPage - 1)}
            >
              Previous
            </button>
            <ul className="flex pt-4 gap-x-4"></ul>
            <button
              disabled={!pokemon.next}
              className="cursor-pointer border-t-2 border-transparent pt-4 pl-1 items-center text-sm font-medium text-gray-500 hover:text-gray-200 hover:border-gray-300"
              onClick={() => setCurrentPage(currentPage + 1)}
            >
              Next
            </button>
          </div>
        </section>
      </main>
    </BaseLayout>
  );
}

export async function getStaticProps(context: GetStaticProps) {
  const response = await axios(
    `https://pokeapi.co/api/v2/pokemon?limit=${per_page}`
  );
  const PokemonList = response.data;
  return {
    props: {
      PokemonList,
    },
  };
}
