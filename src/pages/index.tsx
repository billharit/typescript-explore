import BaseLayout from "@/components/layout/Base";
import type { NextPage } from "next";

const Home: NextPage = () => {
  return (
    <BaseLayout>
      <main>
        <section className="layout min-h-main flex text-6xl items-center justify-center">
          <h1>Typescript Exploration Website</h1>
        </section>
      </main>
    </BaseLayout>
  );
};

export default Home;
